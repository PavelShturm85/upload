"""Test helpers."""
from __future__ import annotations
import io
import typing

import fastapi
from fastapi.testclient import TestClient
from PIL import Image
from platform_jwt_tools import roles
from platform_jwt_tools.tests import helpers as tests_helpers

from tests import mock
from upload import connections, external, settings  # pylint: disable=unused-import
from upload.views import debug as debug_views
from upload.views import main as main_views
from upload.views import settings as settings_views


TEST_USER: roles.User = tests_helpers.generate_public_user()
TEST_PRIVATE_OPERATOR_USER: roles.User = tests_helpers.generate_private_user(roles.PrivateUserTypeEnum.operator)


def generate_image(
    size: int = 200, color: str = 'green', save_format: str = 'JPEG', get_value: bool = False
) -> typing.Union[io.BytesIO, bytes]:
    """Generate image helper."""
    thumb_io: io.BytesIO = io.BytesIO()
    with Image.new(
        'RGB',
        (
            size,
            size,
        ),
        color,
    ) as thumb:
        thumb.save(thumb_io, format=save_format)
    thumb_io.seek(0)
    if get_value:
        thumb_io.getbuffer()
    return thumb_io


def generate_bad_byte_payload() -> typing.Union[io.BytesIO, bytes]:
    """Fake bytes generator."""
    return generate_image(1, save_format='DIB', get_value=True)


def build_fastapi_app():
    """Build fastapi app."""
    app: fastapi.FastAPI = fastapi.FastAPI(docs_url=settings.DOC_PREFIX)
    app.include_router(main_views.ROUTER_OBJ, prefix=settings.API_PREFIX)
    app.include_router(settings_views.ROUTER_OBJ)
    app.include_router(debug_views.ROUTER_OBJ, prefix=settings.API_PREFIX)

    # pylint: disable=unnecessary-lambda
    app.dependency_overrides[connections.RedisPersistentConnection] = lambda: mock.MockRedisDepends()
    app.dependency_overrides[connections.S3ClientConnection] = lambda: mock.build_s3_connection_mock()
    app.dependency_overrides[external.KasperskyScanEngineClient] = lambda: mock.MockKasperskyDepends()
    return app


# pylint: disable=import-outside-toplevel, unnecessary-lambda
def build_fastapi_client():
    """Basic fastapi app client builder."""
    return TestClient(build_fastapi_app())
