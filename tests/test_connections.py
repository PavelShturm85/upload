"""Basic tests."""

import pytest

from tests import mock
from upload import __main__, connections


class MockConnection(connections.BaseSingletonConnection):
    """Mock connections."""

    @classmethod
    async def initialize(cls) -> None:
        """Initialize connection."""

    @classmethod
    async def shutdown(cls) -> None:
        """Close connection."""


def test_is_singleton_connection():
    """Test is singleton connection."""
    one_obj = MockConnection()
    other_obj = MockConnection()
    assert one_obj is other_obj


@pytest.mark.asyncio
async def test_startup_and_shutdown(monkeypatch):
    """Test startup and shutdown."""
    monkeypatch.setattr('upload.connections.aioredis', mock.MockAsyncRedis())
    await __main__.startup()
    await __main__.shutdown()
