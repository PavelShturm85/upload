"""Some things across tests."""
from __future__ import annotations

import pytest
from chat_log_tools import context

from tests import helpers


@pytest.fixture(autouse=True)
def patch_all_environment_vars(monkeypatch):
    """Patch all environments."""
    monkeypatch.setenv('PLATFORM_DEBUG', 'False')
    monkeypatch.setenv('UPLOAD_S3_BUCKET', 'something')
    monkeypatch.setenv('UPLOAD_S3_ENDPOINT', 'ha')
    monkeypatch.setenv('UPLOAD_S3_ACCESS_KEY', 'privet')
    monkeypatch.setenv('UPLOAD_S3_SECRET_KEY', 'kak zhiza?')


@pytest.fixture(scope='module')
def app():
    """App fixture."""
    return helpers.build_fastapi_app()


@pytest.fixture
def fastapi_client():
    """Client for our application."""
    return helpers.build_fastapi_client()


@pytest.fixture(autouse=True)
def inject_request_id():
    """Inject request id into context."""
    context.set_request_id('obviously-fake')
