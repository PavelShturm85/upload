"""Basic tests."""
import typing

import pytest
from fastapi import responses as fastapi_responses

from tests import helpers, mock
from upload import connections, services, settings


GOOD_MIME_TYPES: typing.Dict[str, str] = {
    'image/png': 'png',
    'image/jpeg': 'jpeg',
    'text/plain': 'txt',
}
UPLOAD_FIELD_KEY: str = 'files'
TO_MANY_FILES: int = 40
TO_BIG_IMAGE_SIZE_PIXELS: int = 10000
PRIVATE_UPLOAD_URL: str = f'{settings.API_PREFIX}/private/?to_user=5'
PUBLIC_UPLOAD_URL: str = f'{settings.API_PREFIX}/public/'
DEBUG_URL: str = f'{settings.API_PREFIX}/debug/'


@pytest.mark.parametrize(
    'client_fixture_name,url',
    [('public_fastapi_test_client', PUBLIC_UPLOAD_URL), ('private_operator_fastapi_test_client', PRIVATE_UPLOAD_URL)],
)
def test_upload_via_get(request, client_fixture_name, url):
    """Stupid test."""
    test_client_data = request.getfixturevalue(client_fixture_name)
    assert test_client_data.fastapi_client.get(url,).status_code == 405


@pytest.mark.parametrize(
    'client_fixture_name,url',
    [('public_fastapi_test_client', PUBLIC_UPLOAD_URL), ('private_operator_fastapi_test_client', PRIVATE_UPLOAD_URL)],
)
def test_upload_empty_post(request, client_fixture_name, url):
    """Another stupid test."""
    test_client_data = request.getfixturevalue(client_fixture_name)
    assert test_client_data.fastapi_client.post(url,).status_code == 422


@pytest.mark.parametrize('mime_type', GOOD_MIME_TYPES.keys())
@pytest.mark.parametrize(
    'client_fixture_name,url',
    [('public_fastapi_test_client', PUBLIC_UPLOAD_URL), ('private_operator_fastapi_test_client', PRIVATE_UPLOAD_URL)],
)
def test_upload_with_one_wannabe_file(request, client_fixture_name, url, mime_type):
    """One file upload (should fail)."""
    test_client_data = request.getfixturevalue(client_fixture_name)
    response: fastapi_responses.Response = test_client_data.fastapi_client.post(
        url,
        headers={'Content-Type': 'multipart/form-data',},
        files={'file': ('filename', helpers.generate_bad_byte_payload(), mime_type)},
    )
    assert response.status_code == 400


@pytest.mark.parametrize('mime_type', GOOD_MIME_TYPES.keys())
def test_upload_with_broken_mime(public_fastapi_test_client, mime_type):
    """Incorrect mime/type in bytes data."""
    response: fastapi_responses.Response = public_fastapi_test_client.fastapi_client.post(
        PUBLIC_UPLOAD_URL,
        files=[
            (
                UPLOAD_FIELD_KEY,
                (f'filename.{GOOD_MIME_TYPES[mime_type]}', helpers.generate_bad_byte_payload(), mime_type),
            ),
            (
                UPLOAD_FIELD_KEY,
                (f'filename2.{GOOD_MIME_TYPES[mime_type]}', helpers.generate_bad_byte_payload(), mime_type),
            ),
            (
                UPLOAD_FIELD_KEY,
                (f'filename3.{GOOD_MIME_TYPES[mime_type]}', helpers.generate_bad_byte_payload(), mime_type),
            ),
        ],
    )
    assert response.status_code == 415, response.text


@pytest.mark.parametrize('mime_type', GOOD_MIME_TYPES.keys())
def test_too_many_files(public_fastapi_test_client, mime_type):
    """To many files for upload."""
    response: fastapi_responses.Response = public_fastapi_test_client.fastapi_client.post(
        PUBLIC_UPLOAD_URL,
        files=[
            (
                UPLOAD_FIELD_KEY,
                (f'filename{one_number}.{GOOD_MIME_TYPES[mime_type]}', helpers.generate_bad_byte_payload(), mime_type),
            )
            for one_number in range(TO_MANY_FILES)
        ],
    )
    assert response.status_code == 413, response.text
    assert response.json()['detail']['error'] == 'To many files at once'


@pytest.mark.parametrize('mime_type', GOOD_MIME_TYPES.keys())
def test_upload_without_filename_and_failed_mime(public_fastapi_test_client, mime_type):
    """Empty filename (and bad mime/type)."""
    response: fastapi_responses.Response = public_fastapi_test_client.fastapi_client.post(
        PUBLIC_UPLOAD_URL, files=[(UPLOAD_FIELD_KEY, ('', helpers.generate_bad_byte_payload(), mime_type)),],
    )
    assert response.status_code == 415, response.text
    assert response.json()['detail']['error'] == 'Invalid mime/type'


@pytest.mark.parametrize('mime_type', tuple(GOOD_MIME_TYPES.keys())[:1])
@pytest.mark.parametrize(
    'client_fixture_name,url',
    [('public_fastapi_test_client', PUBLIC_UPLOAD_URL), ('private_operator_fastapi_test_client', PRIVATE_UPLOAD_URL)],
)
def test_too_big_file_upload(request, client_fixture_name, url, mime_type):
    """Test with oversized dynamic file."""
    test_client_data = request.getfixturevalue(client_fixture_name)
    services.PublicUserFileService.max_file_size = 9900
    services.PrivateUserFileService.max_file_size = 9900
    response: fastapi_responses.Response = test_client_data.fastapi_client.post(
        url,
        files=[
            (
                UPLOAD_FIELD_KEY,
                (
                    f'filename1.{GOOD_MIME_TYPES[mime_type]}',
                    helpers.generate_image(TO_BIG_IMAGE_SIZE_PIXELS),
                    mime_type,
                ),
            ),
        ],
    )
    assert response.status_code == 413, response.text


@pytest.mark.parametrize(
    'client_fixture_name,url',
    [('public_fastapi_test_client', PUBLIC_UPLOAD_URL), ('private_operator_fastapi_test_client', PRIVATE_UPLOAD_URL)],
)
def test_with_exception_in_upload(request, client_fixture_name, url):
    """Exception happens during S3 upload."""
    test_client_data = request.getfixturevalue(client_fixture_name)
    test_client_data.fastapi_client.app.dependency_overrides[
        connections.S3ClientConnection
    ] = lambda: mock.build_s3_connection_mock(raiser_class=Exception)
    try:
        response: fastapi_responses.Response = test_client_data.fastapi_client.post(
            url, files=[(UPLOAD_FIELD_KEY, ('filename.txt', b'hohoh kokoko', 'plain/text',),),],
        )
        assert response.status_code == 500, response.text
    finally:
        test_client_data.fastapi_client.app.dependency_overrides[
            connections.S3ClientConnection
        ] = lambda: mock.build_s3_connection_mock()  # pylint: disable=unnecessary-lambda


@pytest.mark.parametrize(
    'client_fixture_name,url',
    [('public_fastapi_test_client', PUBLIC_UPLOAD_URL), ('private_operator_fastapi_test_client', PRIVATE_UPLOAD_URL)],
)
def test_with_plain_text(request, client_fixture_name, url):
    """Test plain text upload."""
    test_client_data = request.getfixturevalue(client_fixture_name)
    response: fastapi_responses.Response = test_client_data.fastapi_client.post(
        url, files=[(UPLOAD_FIELD_KEY, ('filename.txt', 'hohoh kokoko', 'plain/text',),),],
    )
    assert response.status_code == 201, response.text


def test_debug_form_not_in_prod(monkeypatch, public_fastapi_test_client):
    """Check debug form in prod mode (should 404)."""
    monkeypatch.setattr('upload.settings.DEBUG', False)
    assert public_fastapi_test_client.fastapi_client.get(DEBUG_URL).status_code == 404


def test_debug_form(monkeypatch, public_fastapi_test_client):
    """Check debug form in debug mode."""
    monkeypatch.setenv('PLATFORM_DEBUG', 'True')
    response: fastapi_responses.Response = public_fastapi_test_client.fastapi_client.get(DEBUG_URL)
    assert response.status_code == 200


@pytest.mark.parametrize('mime_type_pair', (('image/jpeg', 'JPEG'), ('image/png', 'PNG')))
@pytest.mark.parametrize(
    'client_fixture_name,url',
    [('public_fastapi_test_client', PUBLIC_UPLOAD_URL), ('private_operator_fastapi_test_client', PRIVATE_UPLOAD_URL)],
)
def test_upload_good_images(request, client_fixture_name, url, mime_type_pair):
    """Good basic images case."""
    test_client_data = request.getfixturevalue(client_fixture_name)
    response: fastapi_responses.Response = test_client_data.fastapi_client.post(
        url,
        files=[
            (
                UPLOAD_FIELD_KEY,
                (
                    f'filename.{GOOD_MIME_TYPES[mime_type_pair[0]]}',
                    helpers.generate_image(save_format=mime_type_pair[1]),
                    mime_type_pair[0],
                ),
            ),
            (
                UPLOAD_FIELD_KEY,
                (
                    f'filename2.{GOOD_MIME_TYPES[mime_type_pair[0]]}',
                    helpers.generate_image(save_format=mime_type_pair[1]),
                    mime_type_pair[0],
                ),
            ),
        ],
    )
    assert response.status_code == 201, response.text


@pytest.mark.parametrize(
    'client_fixture_name,url',
    [('public_fastapi_test_client', PUBLIC_UPLOAD_URL), ('private_operator_fastapi_test_client', PRIVATE_UPLOAD_URL)],
)
def test_upload_good_text(request, client_fixture_name, url):
    """Good basic text case."""
    test_client_data = request.getfixturevalue(client_fixture_name)
    response: fastapi_responses.Response = test_client_data.fastapi_client.post(
        url, files=[(UPLOAD_FIELD_KEY, ('filename.txt', b'hohoh kokoko', 'plain/text',),),],
    )
    assert response.status_code == 201, response.text
