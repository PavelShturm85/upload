"""Test main app."""


def test_app_import():
    """Just import the app."""
    # pylint: disable=import-outside-toplevel
    from upload.__main__ import APP_OBJ

    assert APP_OBJ
