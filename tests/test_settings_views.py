"""Settings views tests."""
import json
import random

import pytest
from fastapi import status

from upload import settings


@pytest.mark.parametrize(
    'params',
    [
        dict(values=list(settings.ALLOWED_MIME_TYPES), status_code=status.HTTP_201_CREATED,),
        dict(values=['test_mime'], status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,),
    ],
)
def test_put_mime_types(private_admin_fastapi_test_client, params):
    """Put mime types test."""

    response = private_admin_fastapi_test_client.fastapi_client.put(
        settings.USER_DEFAULT_SETTINGS['mime_types']['endpoint'], data=json.dumps({'values': params['values']}),
    )
    assert response.status_code == params['status_code']


@pytest.mark.parametrize(
    'params',
    [
        dict(
            value=random.randint(
                settings.USER_DEFAULT_SETTINGS['max_size']['values_in_mb']['min'],
                settings.USER_DEFAULT_SETTINGS['max_size']['values_in_mb']['max'],
            ),
            status_code=status.HTTP_201_CREATED,
        ),
        dict(
            value=settings.USER_DEFAULT_SETTINGS['max_size']['values_in_mb']['max'] + 1,
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        ),
    ],
)
def test_put_max_size(private_admin_fastapi_test_client, params):
    """Put mime types test."""

    response = private_admin_fastapi_test_client.fastapi_client.put(
        settings.USER_DEFAULT_SETTINGS['max_size']['endpoint'], data=json.dumps({'value': params['value']}),
    )
    assert response.status_code == params['status_code']


@pytest.mark.parametrize(
    'params', [dict(status_code=status.HTTP_200_OK),],
)
@pytest.mark.parametrize(
    'url',
    [
        settings.USER_DEFAULT_SETTINGS['mime_types']['endpoint'],
        settings.USER_DEFAULT_SETTINGS['max_size']['endpoint'],
        f'{settings.INTERNAL_API_PREFIX}/defaults/',
    ],
)
def test_get_settings(private_admin_fastapi_test_client, params, url):
    """Get mime types test."""

    response = private_admin_fastapi_test_client.fastapi_client.get(url)
    assert response.status_code == params['status_code']
