"""Test helpers."""
from __future__ import annotations
import random
import typing

from upload import settings  # pylint: disable=unused-import


class MockAsyncRedis:
    """Mock async redis."""

    def __init__(self, db_basic: typing.Optional[dict] = None, db_set: typing.Optional[dict] = None):
        self.db_basic: typing.Dict = {} if db_basic is None else db_basic
        self.db_set: typing.Dict = {} if db_set is None else db_set

    async def srem(self, key, *args) -> None:
        """Mock srem method."""
        self.db_set[key] = self.db_set.get(key, set()) - set(args)

    async def smembers(self, key: str) -> set:
        """Mock smembers method."""
        return self.db_set.get(key, set())

    async def sadd(self, key: str, *args) -> None:
        """Mock sadd method."""
        self.db_set[key] = set(args)

    def multi_exec(self) -> object:
        """Mock multi_exec method."""
        return self

    async def execute(self) -> object:
        """Mock execute method."""
        return self

    async def set(self, key: str, value: typing.Optional[typing.Iterable]) -> None:
        """Mock set method."""
        self.db_basic[key] = value

    async def get(self, key: str) -> typing.Any:
        """Mock get method."""
        return self.db_basic.get(key)

    async def create_redis_pool(self, *_, **__) -> object:
        """Mock create pool."""
        return self

    def close(self) -> None:
        """Mock close."""

    async def wait_closed(self) -> None:
        """Mock wait close."""


class MockRedisDepends:
    """Mock kafka depends."""

    connection = MockAsyncRedis(
        db_basic=dict(
            max_size=random.randint(
                settings.USER_DEFAULT_SETTINGS['max_size']['values_in_mb']['min'],
                settings.USER_DEFAULT_SETTINGS['max_size']['values_in_mb']['max'],
            )
        ),
        db_set=dict(mime_types=settings.ALLOWED_MIME_TYPES),
    )


class MockKasperskyDepends:
    """Mock kafka depends."""

    async def scan_file(self, *_, **__) -> None:
        """Mock scan file method."""


class S3ClientMock:
    """Our custom mock."""

    def __init__(self, raiser_class):
        self.raiser_class = raiser_class

    async def put_object(self, *_, **__) -> None:
        """Upload fake."""
        if self.raiser_class:
            raise self.raiser_class()

    async def __aenter__(self) -> 'S3ClientMock':
        """Wannabe mock."""
        return self

    async def __aexit__(self, *_) -> None:
        """Wannabe mock."""


def build_s3_connection_mock(raiser_class: typing.Optional[typing.Type] = None):
    """Flexible mock builder."""

    class S3ClientSingletonMock:
        """S3 client singleton mock."""

        connection = S3ClientMock(raiser_class)

    return S3ClientSingletonMock()
