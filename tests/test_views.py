"""Basic tests."""
import io
import typing
from unittest import mock

import pytest
from fastapi import UploadFile
from fastapi.exceptions import HTTPException

from tests import helpers as test_helpers
from tests import mock as custom_mock
from upload import exceptions, external, models, services
from upload.views import main


@pytest.mark.parametrize(
    'params',
    [
        dict(
            file=test_helpers.generate_image(),
            content_type='image/jpeg',
            file_name='test_1.jpeg',
            is_extension=True,
            is_io_error=True,
        ),
        dict(
            file=io.StringIO('kek'),
            content_type='text/plain',
            file_name='test_2.txt',
            is_extension=False,
            is_io_error=True,
        ),
        dict(file=io.StringIO('kek'), content_type='text/plain', file_name='', is_extension=False, is_io_error=False),
        dict(
            file=io.StringIO('kek'),
            content_type='image/png',
            file_name='test_4.png',
            is_extension=True,
            is_io_error=True,
        ),
    ],
)
@pytest.mark.asyncio
async def test_direct_views_with_bad_str_read(monkeypatch, params):
    """Test wannabe file with read, that returns str."""
    if params['is_io_error']:
        monkeypatch.setattr('PIL.Image.open', mock.Mock(side_effect=IOError))
    if not params['is_extension']:
        monkeypatch.setattr('mimetypes.guess_extension', mock.Mock(return_value=None))

    if params['is_io_error'] and 'jpeg' in params['content_type']:
        with pytest.raises(exceptions.FileUploadError):
            await main.public_user_upload(
                test_helpers.TEST_USER,
                [
                    UploadFile(params['file_name'], params['file'], content_type=params['content_type']),
                ],
                public_upload_view=main.PublicUploadView(
                    public_file_service=services.PublicUserFileService(
                        kaspersky_client=custom_mock.MockKasperskyDepends(),
                        s3_client=external.S3Client(custom_mock.build_s3_connection_mock()),
                    ),
                ),
            )
    else:
        await main.public_user_upload(
            test_helpers.TEST_USER,
            [
                UploadFile(params['file_name'], params['file'], content_type=params['content_type']),
            ],
            public_upload_view=main.PublicUploadView(
                public_file_service=services.PublicUserFileService(
                    kaspersky_client=custom_mock.MockKasperskyDepends(),
                    s3_client=external.S3Client(custom_mock.build_s3_connection_mock()),
                ),
            ),
        )


def fake_upload_build(raise_unicode: bool = False) -> typing.Type:
    """Helper for some error reads."""

    class FakeUpload(UploadFile):
        """Special mock wrapper."""

        async def read(self, size: int = None) -> typing.Union[bytes, str]:  # pylint: disable=W0613
            """Wannabe read."""

            class BrokenUnicodeString(str):
                """Possible unicode error emission."""

                def encode(self, encoding: str = '', errors: str = '') -> bytes:
                    """Raise error."""
                    if raise_unicode:
                        raise UnicodeEncodeError('test', 't', 1, 2, 'asd')
                    return b''

            return BrokenUnicodeString()

    return FakeUpload


@pytest.mark.asyncio
async def test_direct_views_with_bad_str_read_and_unicode_fail():
    """Test wannabe file with read, that returns str."""
    with pytest.raises(HTTPException):
        await main.public_user_upload(
            test_helpers.TEST_USER,
            [
                fake_upload_build(raise_unicode=True)('some_name_1.txt', io.StringIO('xkek1')),
            ],
            public_upload_view=main.PublicUploadView(
                public_file_service=services.PublicUserFileService(
                    kaspersky_client=custom_mock.MockKasperskyDepends(),
                    s3_client=external.S3Client(custom_mock.build_s3_connection_mock()),
                ),
            ),
        )


@pytest.mark.asyncio
@pytest.mark.parametrize('desired_extension', ['txt', 'png', 'jpg'])
async def test_upload_bad_file_name_public(desired_extension):
    """Test upload not unicode file name."""
    some_result: models.GoodResponse = await main.public_user_upload(
        test_helpers.TEST_USER,
        [
            UploadFile(
                '\udcd0\udcba\udcd0\udcbe\udcd1\udc82_cat.' + desired_extension,
                io.StringIO('kek3'),
                content_type='FOR TEH EMPERARH',
            )
        ],
        public_upload_view=main.PublicUploadView(
            public_file_service=services.PublicUserFileService(
                kaspersky_client=custom_mock.MockKasperskyDepends(),
                s3_client=external.S3Client(custom_mock.build_s3_connection_mock()),
            ),
        ),
    )

    assert some_result.files[0].url.endswith('.txt')


@pytest.mark.asyncio
@pytest.mark.parametrize('desired_extension', ['txt', 'png', 'jpg'])
async def test_upload_bad_file_name_private(desired_extension):
    """Test upload not unicode file name."""
    some_result: models.GoodResponse = await main.private_user_upload(
        test_helpers.TEST_USER.user_info.user_id,
        test_helpers.TEST_PRIVATE_OPERATOR_USER,
        [
            UploadFile(
                '\udcd0\udcba\udcd0\udcbe\udcd1\udc82_cat.' + desired_extension,
                test_helpers.generate_image(save_format='JPEG' if desired_extension == 'jpg' else desired_extension)
                if desired_extension != 'txt'
                else io.StringIO('Medved, cho kak?'),
                content_type='brokeh',
            )
        ],
        private_upload_view=main.PrivateUploadView(
            private_file_service=services.PrivateUserFileService(
                s3_client=external.S3Client(custom_mock.build_s3_connection_mock()),
            ),
        ),
    )
    assert some_result.files[0].url.endswith(desired_extension)  # hello to magic mime
