#!/usr/bin/env bash

pylint upload tests
mypy --config-file mypy.ini .
