#!/usr/bin/env bash

uvicorn upload.__main__:APP_OBJ\
    --reload\
    --reload-dir upload\
    --host 0.0.0.0\
    --port ${UPLOAD_APP_PORT:-8011}\
    --log-level debug
