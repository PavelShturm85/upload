#!/usr/bin/env bash

docker build -t platform-upload . --build-arg USER=$USER --build-arg PASSWORD=$PASSWORD --no-cache
