"""Image service."""
import io
import typing

from loguru import logger
from PIL import Image

from upload import exceptions, models


def is_image(content_type: str) -> bool:
    """Check image or not type."""
    return bool('image' in content_type.lower())


def get_image_size(file_data_struct: models.FileDataForUpload) -> typing.Tuple:
    """Get image size."""
    try:
        image: Image.open = Image.open(io.BytesIO(file_data_struct.file_data))
        height, width = image.size
        return height, width
    except IOError as exc:
        logger.exception(f'Can not get size image: {file_data_struct.file_name}.')
        raise exceptions.FileUploadError(file_data_struct.file_name) from exc


def convert_image_to_webp(
    file_data_struct: models.FileDataForUpload,
) -> typing.Tuple[bytes, typing.Optional[int], typing.Optional[int]]:
    """Convert image to webp."""
    try:
        height: typing.Optional[int]
        width: typing.Optional[int]
        with Image.open(io.BytesIO(file_data_struct.file_data)) as img:
            height, width = img.size
            with io.BytesIO() as buffer:
                img.save(buffer, 'WEBP')
                buffer.seek(0)
                return buffer.getvalue(), height, width
    except IOError as exc:
        logger.exception(f'Can not convert image {file_data_struct.file_name} to WEBP.')
        raise exceptions.FileUploadError(file_data_struct.file_name) from exc
