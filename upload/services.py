"""Upload services."""
from __future__ import annotations
import abc
import hashlib
import secrets
import typing

import magic
from fastapi import Depends
from loguru import logger

from upload import exceptions, external, image, models, settings


class FileServiceProtocol(typing.Protocol):
    """Protocol for file services."""

    @abc.abstractmethod
    async def upload_file(self, file_name: str, file_data: bytes, user_id: str) -> models.UploadedFileData:
        """Upload file."""
        raise NotImplementedError()


# pylint: disable=raise-missing-from
class BaseUserFileService:
    """Base user file service."""

    allowed_mime_types: typing.Optional[typing.Union[typing.Iterable, str]] = None
    s3_bucket_name: typing.Optional[str] = None
    max_file_size: typing.Optional[int] = None
    max_image_size: typing.Optional[int] = None

    def __init__(self, s3_client=Depends(external.S3Client)):
        assert self.allowed_mime_types, 'You should set `allowed_mime_types` class field'
        assert self.s3_bucket_name, 'You should set `s3_bucket_name` class field'
        assert self.max_file_size, 'You should set `max_file_size` class field'
        assert self.max_image_size, 'You should set `max_image_size` class field'
        self.s3_client: external.S3Client = s3_client.init_bucket_name(self.s3_bucket_name)

    def check_mime_type(self, file_data: bytes):
        """Check mime type."""
        real_mime_type: str = magic.from_buffer(file_data, mime=True)
        if self.allowed_mime_types == '__all__':
            return real_mime_type, True
        return real_mime_type, bool(real_mime_type in self.allowed_mime_types)  # type: ignore

    def _parse_file_info(self, file_name: str, file_obj: bytes, user_id: str) -> models.FileDataForUpload:
        """Parse file info."""
        real_mime_type: str
        is_valid: bool
        real_mime_type, is_valid = self.check_mime_type(file_obj)

        if not is_valid:
            logger.warning(f'Invalid mimetype: file {file_name} has [{real_mime_type}], which is not allowed')
            raise exceptions.FileDoesNotSupportedError(file_name=file_name)

        file_size: int = len(file_obj)
        if ('image' in real_mime_type.lower() and file_size > self.max_image_size) or (  # type: ignore
            file_size > self.max_file_size  # type: ignore
        ):
            raise exceptions.FileTooLargeError(file_name)

        return models.FileDataForUpload(
            file_data=file_obj, file_name=file_name, content_type=real_mime_type, user_id=user_id, file_size=file_size
        )

    def _generate_file_name(
        self, file_data_struct: models.FileDataForUpload, override_extension: typing.Optional[str] = None
    ) -> str:
        """Generate file name for s3."""
        result_file_name: str
        hash_for_filename: typing.Callable = hashlib.sha3_224
        rand_secret_bytes: bytes = secrets.token_bytes(256)
        if not file_data_struct.file_name:
            logger.warning('File name has not provided')
            result_file_name = hash_for_filename(rand_secret_bytes).hexdigest()
        else:
            encoded_file_name: bytes = b''
            try:
                encoded_file_name = file_data_struct.file_name.encode('utf-8')
            except UnicodeEncodeError:
                logger.warning(f'Cant convert original file name to unicode: {file_data_struct.file_name}')
            result_file_name = hash_for_filename(rand_secret_bytes + encoded_file_name).hexdigest()
        filename_suffix: typing.Optional[str] = override_extension or settings.KNOWN_MIMES_TO_EXTENSIONS.get(
            file_data_struct.content_type, ''
        )

        return f'{result_file_name}.{filename_suffix}'

    @abc.abstractmethod
    async def _prepare_for_upload(
        self, file_data_struct: models.FileDataForUpload
    ) -> typing.Tuple[bytes, typing.Optional[int], typing.Optional[int], typing.Optional[str]]:
        """Prepare data for upload."""
        raise NotImplementedError()

    async def upload_file(self, file_name: str, file_data: bytes, user_id: str) -> models.UploadedFileData:
        """Private upload file function."""
        width: typing.Optional[int]
        height: typing.Optional[int]
        override_suffix: typing.Optional[str]
        file_data_struct: models.FileDataForUpload = self._parse_file_info(file_name, file_data, user_id)
        file_data_struct.file_data, width, height, override_suffix = await self._prepare_for_upload(file_data_struct)
        s3_file_name: str = await self.s3_client.upload_file(
            file_data_struct, self._generate_file_name(file_data_struct, override_suffix)
        )
        return models.UploadedFileData(
            file_name_for_s3=s3_file_name,
            content_type=file_data_struct.content_type,
            file_size=file_data_struct.file_size,
            file_name=file_data_struct.file_name,
            width=width,
            height=height,
        )


class PrivateUserFileService(BaseUserFileService):
    """Private user file service."""

    allowed_mime_types: str = '__all__'
    s3_bucket_name: str = settings.S3_PRIVATE_BUCKET
    max_file_size: int = settings.MAXIMUM_PRIVATE_FILE_SIZE_BYTES
    max_image_size: int = settings.MAXIMUM_IMAGE_SIZE_BYTES

    async def _prepare_for_upload(
        self, file_data_struct: models.FileDataForUpload
    ) -> typing.Tuple[bytes, typing.Optional[int], typing.Optional[int], typing.Optional[str]]:
        """Prepare data for upload."""
        height: typing.Optional[int] = None
        width: typing.Optional[int] = None
        if image.is_image(file_data_struct.content_type):
            width, height = image.get_image_size(file_data_struct)

        return file_data_struct.file_data, width, height, None


class PublicUserFileService(BaseUserFileService):
    """Public user file service."""

    allowed_mime_types: typing.Iterable = settings.ALLOWED_MIME_TYPES
    s3_bucket_name: str = settings.S3_PUBLIC_BUCKET
    max_file_size: int = settings.MAXIMUM_PUBLIC_FILE_SIZE_BYTES
    max_image_size: int = settings.MAXIMUM_IMAGE_SIZE_BYTES

    def __init__(
        self,
        kaspersky_client=Depends(external.KasperskyScanEngineClient),
        s3_client=Depends(external.S3Client),
    ):
        super().__init__(s3_client)
        self.kaspersky_client: external.KasperskyScanEngineClient = kaspersky_client

    async def _prepare_for_upload(
        self, file_data_struct: models.FileDataForUpload
    ) -> typing.Tuple[bytes, typing.Optional[int], typing.Optional[int], typing.Optional[str]]:
        height: typing.Optional[int] = None
        width: typing.Optional[int] = None
        file_extension: typing.Optional[str] = None
        file_data: bytes = file_data_struct.file_data
        if image.is_image(file_data_struct.content_type):
            new_file_data, width, height = image.convert_image_to_webp(file_data_struct)
            file_data = new_file_data
            file_extension = 'webp'
        else:
            await self.kaspersky_client.scan_file(file_data_struct.file_data)
        return file_data, width, height, file_extension
