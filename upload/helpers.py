"""Helpers module."""
import typing

import aioredis
import backoff

from upload import settings


@backoff.on_exception(
    backoff.expo,
    (aioredis.WatchVariableError, aioredis.MultiExecError, aioredis.PipelineError),
    max_tries=settings.DEFAULT_RETRY_COUNT,
)
async def redis_set_override(redis_connection: aioredis.Redis, set_key: str, new_set_state: typing.Set):
    """Override set state in Redis (in transaction)"""
    old_set_state: typing.Set = await redis_connection.smembers(set_key)
    override_transaction: aioredis.commands.transaction.MultiExec = redis_connection.multi_exec()
    if old_set_state:
        override_transaction.srem(set_key, *old_set_state)
    if new_set_state:
        override_transaction.sadd(set_key, *new_set_state)
    await override_transaction.execute()
