"""Upload exception."""
import typing


class FileUploadError(Exception):
    """File upload basic error."""

    def __init__(self, file_name: typing.Optional[str], *args: typing.Any):
        self.file_name: typing.Optional[str] = file_name
        super().__init__(*args)


class FileTooLargeError(FileUploadError):
    """Error when file is too large."""


class FileDoesNotSupportedError(FileUploadError):
    """Error when file does not supported."""
