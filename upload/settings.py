"""Settings file for Platform-upload project."""
from __future__ import annotations
import typing

import envparse


DEBUG: bool = envparse.env('PLATFORM_DEBUG', 'True', cast=bool)
PERSISTENT_REDIS_DSN: str = envparse.env('UPLOAD_REDIS_DSN', 'redis://127.0.0.1:6379/0')
KAFKA_URL: str = envparse.env('PLATFORM_KAFKA_DSN', 'localhost:9092')
KAFKA_TOPIC_FOR_UPLOADED_FILE: str = envparse.env('PLATFORM_KAFKA_TOPIC_FOR_UPLOADED_FILE', '')
API_PREFIX: str = envparse.env('UPLOAD_API_PREFIX', '/api/upload/').rstrip('/')
INTERNAL_API_PREFIX: str = envparse.env('UPLOAD_API_INTERNAL_PREFIX', f'{API_PREFIX}/manage', cast=str)
DOC_PREFIX: str = envparse.env('UPLOAD_DOC_PREFIX', '/doc/api/service-upload/').rstrip('/') + '/'
BULK_FILES_COUNT: int = envparse.env('UPLOAD_BULK_FILES_COUNT', 10, cast=int)
MAXIMUM_PUBLIC_FILE_SIZE_BYTES: int = envparse.env('UPLOAD_MAXIMUM_PUBLIC_FILE_SIZE_BYTES', 10 * 1024 * 1024, cast=int)
MAXIMUM_PRIVATE_FILE_SIZE_BYTES: int = envparse.env(
    'UPLOAD_MAXIMUM_PRIVATE_FILE_SIZE_BYTES', 50 * 1024 * 1024, cast=int
)
MAXIMUM_IMAGE_SIZE_BYTES: int = envparse.env('UPLOAD_MAXIMUM_IMAGE_SIZE_BYTES', 10 * 1024 * 1024, cast=int)
S3_PUBLIC_BUCKET: str = envparse.env('PLATFORM_S3_PUBLIC_BUCKET', 'test-public', cast=str)
S3_PRIVATE_BUCKET: str = envparse.env('PLATFORM_S3_PRIVATE_BUCKET', 'test-private', cast=str)
S3_WEB_SERVER_URL: str = envparse.env('PLATFORM_S3_WEB_SERVER_VISIBLE_URL', 'http://localhost/storage-fetch/')
S3_ENDPOINT: str = envparse.env('PLATFORM_S3_ENDPOINT', 'http://127.0.0.1:9060', cast=str)
S3_ACCESS_KEY: str = envparse.env('PLATFORM_S3_ACCESS_KEY', 'minioadmin', cast=str)
S3_SECRET_KEY: str = envparse.env('PLATFORM_S3_SECRET_KEY', 'minioadmin', cast=str)
HTTP_SESSION_CONNECTION_TRIES: int = envparse.env('UPLOAD_HTTP_SESSION_CONNECTION_TRIES', cast=int, default=3)
KASPERSKY_SCAN_ENGINE_URL: str = envparse.env(
    'UPLOAD_KASPERSKY_SCAN_ENGINE_URL', 'http://127.0.0.1:9998/api/v3.0/scanmemory'
)
KASPERSKY_SCAN_ENGINE_TIMEOUT: str = envparse.env('UPLOAD_KASPERSKY_SCAN_ENGINE_TIMEOUT', '10000')
VERIFY_SSL: bool = envparse.env('UPLOAD_VERIFY_SSL', False, cast=bool)

TAGS_METADATA: list = [
    {"name": "main", "description": "Upload methods for private/public users"},
    {"name": "settings", "description": "Methods that return some service setting value"},
    {"name": "debug", "description": "Debug methods"},
    {"name": "health", "description": "Service heath check"},
]

JWT_PUBLIC_KEY: str = envparse.env(
    'PLATFORM_JWT_PUBLIC_KEY',
    cast=str,
    default='',
)

DEFAULT_RETRY_COUNT: int = envparse.env('UPLOAD_RETRY_COUNT', 3, cast=int)
USER_DEFAULT_SETTINGS: typing.Dict = {
    'mime_types': {
        'endpoint': f'{INTERNAL_API_PREFIX}/mime_types/',  # --> api_post_mime
        'values': {
            'doc': {'mime': 'application/msword', 'need_to_persistent_threat_scan': True},
            'docx': {
                'mime': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'need_to_persistent_threat_scan': True,
            },
            'xls': {'mime': 'application/vnd.ms-excel', 'need_to_persistent_threat_scan': True},
            'xlsx': {
                'mime': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'need_to_persistent_threat_scan': True,
            },
            'pdf': {'mime': 'application/pdf', 'need_to_persistent_threat_scan': True},
            'txt': {'mime': 'text/plain', 'need_to_persistent_threat_scan': True},
            'rtf': {'mime': 'text/rtf', 'need_to_persistent_threat_scan': True},
            'jpeg': {'mime': 'image/jpeg', 'need_to_persistent_threat_scan': False},
            'jpg': {'mime': 'image/jpeg', 'need_to_persistent_threat_scan': False},
            'png': {'mime': 'image/png', 'need_to_persistent_threat_scan': False},
        },
    },
    'max_size': {
        'endpoint': f'{INTERNAL_API_PREFIX}/max_size/',
        'values_in_mb': {'min': 1.0, 'max': 20.0},
    },  # --> api_post_max_size
}

KNOWN_MIMES_TO_EXTENSIONS: dict = {
    inner_map['mime']: extension for extension, inner_map in USER_DEFAULT_SETTINGS['mime_types']['values'].items()
}

ALLOWED_MIME_TYPES: typing.Set[str] = {
    USER_DEFAULT_SETTINGS['mime_types']['values'][key]['mime'] for key in USER_DEFAULT_SETTINGS['mime_types']['values']
}
SENTRY_DSN: str = envparse.env(
    'UPLOAD_SENTRY_DSN',
    cast=str,
    default='',
)
S3_CONNECTION_TRIES: int = envparse.env('UPLOAD_S3_CONNECTION_TRIES', cast=int, default=3)
SCAN_STATUS_KEY: str = envparse.env(
    'PLATFORM_SCAN_STATUS_KEY',
    'scan_status',
    cast=str,
)
SCAN_DONE_STATUS: str = envparse.env('PLATFORM_SCAN_DONE_STATUS', 'scan_done', cast=str)

UVICORN: dict = {
    'APP_PORT': envparse.env('UPLOAD_APP_PORT', default=9991, cast=int),
    'LOG_LEVEL': envparse.env('PLATFORM_LOG_LEVEL', 'info'),
    'APP_WORKERS': envparse.env('UPLOAD_APP_WORKERS', default=3, cast=int),
    'APP_LIMIT_MAX_REQUESTS': envparse.env('UPLOAD_APP_LIMIT_MAX_REQUESTS', default=3000, cast=int),
    'APP_LOOP': envparse.env('UPLOAD_APP_LOOP', 'uvloop'),
}
