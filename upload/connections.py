"""Connections."""

import contextlib
import typing
from abc import ABC, abstractmethod

import aioboto3
import aiohttp.client_exceptions
import aioredis
import backoff

from upload import settings


class BaseSingletonConnection(ABC):
    """Base singleton connection."""

    connection: typing.Optional[typing.Any] = None

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super().__new__(cls)
        return cls.instance

    @classmethod
    @abstractmethod
    async def initialize(cls) -> None:
        """Initialize connection."""

    @classmethod
    @abstractmethod
    async def shutdown(cls) -> None:
        """Close connection."""


class RedisPersistentConnection(BaseSingletonConnection):
    """Redis persistent connection."""

    @classmethod
    async def initialize(cls):
        """Initialize redis persistent connection."""
        cls.connection: aioredis.ConnectionsPool = await aioredis.create_redis_pool(settings.PERSISTENT_REDIS_DSN)

    @classmethod
    async def shutdown(cls) -> None:
        """Close redis persistent connection."""
        if cls.connection:
            cls.connection.close()
            await cls.connection.wait_closed()


class S3ClientConnection(BaseSingletonConnection):
    """Singleton for s3 client."""

    _context_stack: typing.Optional[contextlib.AsyncExitStack] = None

    @classmethod
    @backoff.on_exception(
        backoff.expo, aiohttp.client_exceptions.ClientConnectorError, max_tries=settings.S3_CONNECTION_TRIES,
    )
    async def initialize(cls):
        """Initialize s3 client."""
        cls._context_stack = contextlib.AsyncExitStack()
        cls.connection = await cls._context_stack.enter_async_context(
            aioboto3.client(
                's3',
                endpoint_url=settings.S3_ENDPOINT,
                aws_access_key_id=settings.S3_ACCESS_KEY,
                aws_secret_access_key=settings.S3_SECRET_KEY,
            )
        )

    @classmethod
    async def shutdown(cls) -> None:
        """Close s3 client."""
        if cls._context_stack:
            await cls._context_stack.aclose()


class AioHttpConnection(BaseSingletonConnection):
    """AioHttp connection."""

    connection: aiohttp.ClientSession

    async def initialize(cls):
        """Initialize aiohttp connection."""
        cls.connection: aiohttp.ClientSession = aiohttp.ClientSession()

    @classmethod
    async def shutdown(cls) -> None:
        """Close aiohttp connection."""
        if cls.connection:
            await cls.connection.close()
