"""External communications logic lies here."""
from __future__ import annotations
import base64
import datetime
import typing

import aiohttp
import aiohttp.client_exceptions
import backoff
from fastapi import Depends
from loguru import logger

from upload import connections, exceptions, models, settings


class KasperskyUnexpectedResponse(Exception):
    """Exception on wrong answer from kaspersky scan engine service."""


class ThreatDetected(Exception):
    """Exception on tread detected."""


class KasperskyScanEngineClient:
    """Kaspersky scan engine client."""

    def __init__(self, aiohttp_connection=Depends(connections.AioHttpConnection)):
        self._aiohttp_connection: aiohttp.ClientSession = aiohttp_connection.connection

    @backoff.on_exception(
        backoff.expo,
        (aiohttp.client_exceptions.ServerConnectionError, aiohttp.client_exceptions.ClientResponseError),
        max_tries=settings.HTTP_SESSION_CONNECTION_TRIES,
    )
    async def _send_request_with_json_reply(self, data: str) -> typing.Dict:
        """Base send message."""
        async with self._aiohttp_connection.post(
            settings.KASPERSKY_SCAN_ENGINE_URL, ssl=settings.VERIFY_SSL, data=data
        ) as response:
            json_response: typing.Dict = await response.json(content_type=None)
            response_status_code: int = int(response.status)
            if response_status_code != 200:
                raise KasperskyUnexpectedResponse(
                    f'Received response: {json_response} from kaspersky, expected status 200',
                )
            return json_response

    async def scan_file(self, file_data: bytes) -> None:
        """Antivirus check."""
        response: dict = await self._send_request_with_json_reply(
            models.KasperskyScanEngineData(object=base64.b64encode(file_data).decode()).json()
        )
        if response.get('scanResult') == 'DETECT':
            tread_detected_message: str = f'Kaspersky detected tread: {response}.'
            logger.warning(tread_detected_message)
            raise ThreatDetected(tread_detected_message)


class S3Client:
    """S3 service."""

    bucket_name: typing.Optional[str] = None

    def __init__(
        self,
        s3_connection=Depends(connections.S3ClientConnection),
    ):
        self._s3_connection: typing.Any = s3_connection.connection

    def init_bucket_name(self, bucket_name: typing.Optional[str]) -> S3Client:
        """Init allowed mime types."""
        self.bucket_name: typing.Optional[str] = bucket_name
        return self

    async def upload_file(self, file_data: models.FileDataForUpload, file_name_for_s3: str) -> str:
        """Upload file to s3."""
        blob_s3_key: str = '{}/{}'.format(datetime.date.today().strftime('%d-%m-%Y'), file_name_for_s3)
        try:
            logger.info(f'Uploading {blob_s3_key} to s3')
            await self._s3_connection.put_object(
                Body=file_data.file_data,
                Bucket=self.bucket_name,
                Key=blob_s3_key,
                ContentType=file_data.content_type,
                Metadata={settings.SCAN_STATUS_KEY: settings.SCAN_DONE_STATUS, 'user_id': file_data.user_id},
            )
            logger.info(f'Finished Uploading {blob_s3_key} to s3')
            return '/'.join(
                (
                    settings.S3_WEB_SERVER_URL.rstrip('/'),
                    self.bucket_name,  # type: ignore
                    blob_s3_key.lstrip('/'),
                )
            )

        except Exception as exc:  # pylint: disable=broad-except
            unable_s3_upload_message: str = f'Unable to s3 upload {file_data.file_name} to {blob_s3_key}'
            logger.exception(unable_s3_upload_message)
            raise exceptions.FileUploadError(file_data.file_name, unable_s3_upload_message) from exc
