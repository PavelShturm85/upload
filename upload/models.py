"""Pydantic models here."""
from __future__ import annotations
import typing

import orjson
import pydantic
from loguru import logger

from upload import settings


def orjson_dumps(obj: typing.Any, *, default: typing.Any,) -> typing.Any:
    """orjson.dumps returns bytes, to match standard json.dumps we need to
    decode."""
    return orjson.dumps(obj, default=default).decode()


class BadResponse(pydantic.BaseModel):
    """Generic bad response."""

    error: str


class BadFileResponse(BadResponse):
    """Bad response for file."""

    filename: str


class PayloadForAntivirusScan(pydantic.BaseModel):
    """Model for payload to antivirus scan."""

    bucket: str
    object_key: str


class AntivirusScan(pydantic.BaseModel):
    """Model for antivirus scan."""

    request_id: str
    payload: PayloadForAntivirusScan
    jwt_token: str


class OneUpload(pydantic.BaseModel):
    """Model for one upload."""

    url: str
    width: typing.Optional[int] = None
    height: typing.Optional[int] = None
    mime_type: typing.Optional[str] = None
    file_size: typing.Optional[int] = None
    file_name: typing.Optional[str] = None


class GoodResponse(pydantic.BaseModel):
    """Good response."""

    files: typing.List[OneUpload]


class MaxUploadSize(pydantic.BaseModel):
    """Max upload size value."""

    value: float

    @pydantic.validator('value')
    def validate_max_upload_size_value(cls, upload_size: float):  # pylint: disable=no-self-use, no-self-argument
        """Validate max upload size correctness."""
        allowable_interval: typing.Dict = settings.USER_DEFAULT_SETTINGS['max_size']['values_in_mb']
        if upload_size < allowable_interval['min'] or upload_size > allowable_interval['max']:
            upload_size_error_message: str = f'Size not in allowed interval: {allowable_interval}.'
            logger.error(upload_size_error_message)
            raise ValueError(upload_size_error_message)

        return upload_size


class MimeTypes(pydantic.BaseModel):
    """Mime types values."""

    values: typing.Set[str]

    @pydantic.validator('values')
    def validate_mime_types_values(cls, mime_types: typing.Set):  # pylint: disable=no-self-use, no-self-argument
        """Validate mime type correctness."""
        if not mime_types.issubset(settings.ALLOWED_MIME_TYPES):
            not_allowed: set = mime_types - settings.ALLOWED_MIME_TYPES
            mime_types_error_message: str = f'Not all MIME types are allowed. {not_allowed}'
            logger.error(mime_types_error_message)
            raise ValueError(mime_types_error_message)
        return mime_types


class DefaultsMimeTypes(pydantic.BaseModel):
    """Defaults mime type."""

    endpoint: str = settings.USER_DEFAULT_SETTINGS['mime_types']['endpoint']
    values: typing.Dict[str, typing.Dict[str, typing.Union[bool, str]]]


class ValuesInMB(pydantic.BaseModel):
    """Values in mb."""

    max: float = settings.USER_DEFAULT_SETTINGS['max_size']['values_in_mb']['max']
    min: float = settings.USER_DEFAULT_SETTINGS['max_size']['values_in_mb']['min']


class DefaultsMaxSize(pydantic.BaseModel):
    """Defaults max size."""

    endpoint: str = settings.USER_DEFAULT_SETTINGS['max_size']['endpoint']
    values_in_mb: ValuesInMB


class UserDefaultsSettings(pydantic.BaseModel):
    """User defaults settings."""

    mime_types: DefaultsMimeTypes
    max_size: DefaultsMaxSize


class KasperskyScanEngineData(pydantic.BaseModel):
    """Kaspersky scan engine."""

    timeout: str = settings.KASPERSKY_SCAN_ENGINE_TIMEOUT
    object: str

    # pylint: disable=missing-class-docstring
    class Config:
        json_loads = orjson.loads
        json_dumps = orjson_dumps


class UploadedFileData(pydantic.BaseModel):
    """Uploaded file data."""

    file_name_for_s3: str
    content_type: typing.Optional[str] = None
    file_size: typing.Optional[int] = None
    file_name: typing.Optional[str] = None
    width: typing.Optional[int] = None
    height: typing.Optional[int] = None


class FileDataForUpload(pydantic.BaseModel):
    """Prepared file data for upload."""

    file_data: bytes
    content_type: str
    user_id: str
    file_size: typing.Optional[int] = None
    file_name: typing.Optional[str] = None
