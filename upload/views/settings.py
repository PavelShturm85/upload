"""Platform-upload project settings entrypoint."""
import typing

import fastapi
from fastapi import Depends, status
from platform_jwt_tools import authentication, permissions

from upload import connections, helpers, models, settings


ROUTER_OBJ: fastapi.APIRouter = fastapi.APIRouter()


@ROUTER_OBJ.get(f'{settings.INTERNAL_API_PREFIX}/defaults/', response_model=models.UserDefaultsSettings)
async def api_get_defaults(
    _=authentication.ChatAuthentication(user_permissions=[permissions.IsAdmin()]),
) -> models.UserDefaultsSettings:
    """Get defaults view."""
    return models.UserDefaultsSettings(**settings.USER_DEFAULT_SETTINGS)


@ROUTER_OBJ.put(
    settings.USER_DEFAULT_SETTINGS['mime_types']['endpoint'],
    status_code=status.HTTP_201_CREATED,
    response_model=models.MimeTypes,
)
async def api_put_mime(
    mime_types: models.MimeTypes,
    redis_persistent=Depends(connections.RedisPersistentConnection),
    _=authentication.ChatAuthentication(user_permissions=[permissions.IsAdmin()]),
) -> models.MimeTypes:
    """Add mime type in redis view."""
    await helpers.redis_set_override(redis_persistent.connection, 'mime_types', mime_types.values)
    return mime_types


@ROUTER_OBJ.get(
    settings.USER_DEFAULT_SETTINGS['mime_types']['endpoint'],
    status_code=status.HTTP_200_OK,
    response_model=models.MimeTypes,
)
async def api_get_mime(
    redis_persistent=Depends(connections.RedisPersistentConnection),
    _=authentication.ChatAuthentication(user_permissions=[permissions.IsAdmin()]),
) -> models.MimeTypes:
    """Add mime type in redis view."""
    mime_types: typing.Set = await redis_persistent.connection.smembers('mime_types')
    return models.MimeTypes(values=mime_types)


@ROUTER_OBJ.put(
    settings.USER_DEFAULT_SETTINGS['max_size']['endpoint'],
    status_code=status.HTTP_201_CREATED,
    response_model=models.MaxUploadSize,
)
async def api_put_max_size(
    max_upload_size: models.MaxUploadSize,
    redis_persistent=Depends(connections.RedisPersistentConnection),
    _=authentication.ChatAuthentication(user_permissions=[permissions.IsAdmin()]),
) -> models.MaxUploadSize:
    """Add max size in redis view."""
    await redis_persistent.connection.set('max_size', max_upload_size.value)
    return max_upload_size


@ROUTER_OBJ.get(
    settings.USER_DEFAULT_SETTINGS['max_size']['endpoint'],
    status_code=status.HTTP_200_OK,
    response_model=models.MaxUploadSize,
)
async def api_get_max_size(
    redis_persistent=Depends(connections.RedisPersistentConnection),
    _=authentication.ChatAuthentication(user_permissions=[permissions.IsAdmin()]),
) -> models.MaxUploadSize:
    """Add max size in redis view."""
    max_upload_size: float = await redis_persistent.connection.get('max_size')
    return models.MaxUploadSize(value=max_upload_size)
