"""Platform-upload project main entrypoint."""
import typing

import fastapi
from fastapi import Depends, Query, status
from platform_jwt_tools import authentication, permissions, roles

from upload import exceptions, external, models, services, settings


ROUTER_OBJ: fastapi.APIRouter = fastapi.APIRouter()


# pylint: disable=raise-missing-from
class BaseUploadView:
    """Base class for s3 upload service."""

    def __init__(
        self, file_service: services.FileServiceProtocol,
    ):
        self.file_service: services.FileServiceProtocol = file_service

    async def _prepare_file_data(self, upload_file: fastapi.UploadFile) -> bytes:
        """Prepare file data."""
        file_data: typing.Union[bytes, str] = await upload_file.read()
        if isinstance(file_data, str):
            try:
                encoded_file_data: bytes = file_data.encode()
            except UnicodeEncodeError:
                raise fastapi.HTTPException(
                    status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                    detail=models.BadFileResponse(filename=upload_file.filename, error='Invalid file encoding'),
                )
        else:
            encoded_file_data = file_data
        return encoded_file_data

    async def upload_file(self, files: typing.List[fastapi.UploadFile], public_user_id: str):
        """User upload functionality."""
        uploaded_files: typing.List[models.OneUpload] = []

        for one_file in files:
            prepared_file_data: bytes = await self._prepare_file_data(one_file)
            try:
                uploaded_file: models.UploadedFileData = await self.file_service.upload_file(
                    one_file.filename, prepared_file_data, public_user_id
                )
            except exceptions.FileDoesNotSupportedError as exc:
                raise fastapi.HTTPException(
                    status_code=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE,
                    detail=models.BadFileResponse(filename=exc.file_name, error='Invalid mime/type').dict(),
                )
            except exceptions.FileTooLargeError as exc:
                raise fastapi.HTTPException(
                    status_code=status.HTTP_413_REQUEST_ENTITY_TOO_LARGE,
                    detail=models.BadFileResponse(filename=exc.file_name, error='File size is more than limit').dict(),
                )
            except external.ThreatDetected:
                raise fastapi.HTTPException(
                    status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                    detail=models.BadFileResponse(filename=one_file.filename, error='Threat detected').dict(),
                )
            except UnicodeEncodeError:
                raise fastapi.HTTPException(
                    status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                    detail=models.BadFileResponse(filename=one_file.filename, error='Invalid file name encoding'),
                )

            uploaded_files.append(
                models.OneUpload(
                    url=uploaded_file.file_name_for_s3,
                    width=uploaded_file.width,
                    height=uploaded_file.height,
                    mime_type=uploaded_file.content_type,
                    file_size=uploaded_file.file_size,
                    file_name=one_file.filename,
                )
            )
        return uploaded_files


class PublicUploadView(BaseUploadView):
    """S3 upload service for public users."""

    def __init__(self, public_file_service: services.PublicUserFileService = Depends(services.PublicUserFileService)):
        super().__init__(public_file_service)

    async def upload_file(self, files: typing.List[fastapi.UploadFile], public_user_id: str):
        """Check count files count before upload."""
        if len(files) > settings.BULK_FILES_COUNT:
            raise fastapi.HTTPException(
                status_code=status.HTTP_413_REQUEST_ENTITY_TOO_LARGE,
                detail=models.BadResponse(error='To many files at once').dict(),
            )
        return await super().upload_file(files, public_user_id)


class PrivateUploadView(BaseUploadView):
    """S3 upload service for private user."""

    def __init__(
        self, private_file_service: services.PrivateUserFileService = Depends(services.PrivateUserFileService)
    ):
        super().__init__(private_file_service)


@ROUTER_OBJ.post("/public/", status_code=status.HTTP_201_CREATED, response_model=models.GoodResponse)
async def public_user_upload(
    user: roles.User = authentication.ChatAuthentication(user_permissions=[permissions.IsPublicUser()]),
    files: typing.List[fastapi.UploadFile] = fastapi.File(...),
    public_upload_view: PublicUploadView = Depends(PublicUploadView),
) -> typing.Any:
    """Core upload view.

    Gets one or more files in array, returns urls for each of them in
    those order, that was in the request.
    """
    uploaded_files: typing.List[models.OneUpload] = await public_upload_view.upload_file(files, user.user_info.user_id)
    return models.GoodResponse(files=uploaded_files)


@ROUTER_OBJ.post("/private/", status_code=status.HTTP_201_CREATED, response_model=models.GoodResponse)
async def private_user_upload(
    to_user: str = Query(...),
    _=authentication.ChatAuthentication(user_permissions=[permissions.IsPrivateUser()]),
    files: typing.List[fastapi.UploadFile] = fastapi.File(...),
    private_upload_view: PrivateUploadView = Depends(PrivateUploadView),
):
    """Private user upload endpoint."""
    uploaded_files: typing.List[models.OneUpload] = await private_upload_view.upload_file(files, to_user)
    return models.GoodResponse(files=uploaded_files)
