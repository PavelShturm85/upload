"""Platform-upload project debug endpoints."""
import fastapi
from fastapi import responses, status

from upload import settings


ROUTER_OBJ: fastapi.APIRouter = fastapi.APIRouter()
DEBUG_FORM: str = f"""
<body style="margin: 20px auto; width: 300px">
    <h1>Test upload</h1>
    <form action=\"{settings.API_PREFIX}/\" enctype="multipart/form-data" method="post">
        <input name="files" type="file" multiple>
        <input type="submit">
    </form>
</body>
"""


@ROUTER_OBJ.get('/debug/')
async def debug_view() -> responses.HTMLResponse:
    """Debug view for test and dev purposes."""
    if not settings.DEBUG:
        raise fastapi.exceptions.HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    return responses.HTMLResponse(content=DEBUG_FORM)
