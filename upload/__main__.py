"""Platform-upload project entrypoint service file."""

import fastapi
import sentry_sdk
from chat_log_tools import middleware as log_middleware
from chat_log_tools import setup_json_logger
from fastapi import status
from health_checks.api.fastapi import get_health_check_router
from platform_jwt_tools import security
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware
from starlette.middleware.cors import CORSMiddleware
from uvicorn import Config, Server

from upload import connections, error_handlers, settings
from upload.views import debug as debug_views
from upload.views import main as main_views
from upload.views import settings as settings_views


async def startup():
    """Global initialization."""
    await connections.RedisPersistentConnection.initialize()
    await connections.S3ClientConnection.initialize()
    await connections.AioHttpConnection.initialize()


async def shutdown():
    """Global shutdown."""
    await connections.RedisPersistentConnection.shutdown()
    await connections.S3ClientConnection.shutdown()
    await connections.AioHttpConnection.shutdown()


if settings.SENTRY_DSN:
    sentry_sdk.init(dsn=settings.SENTRY_DSN)

APP_OBJ: fastapi.FastAPI = fastapi.FastAPI(
    docs_url=settings.DOC_PREFIX,
    on_startup=[startup],
    on_shutdown=[shutdown],
    openapi_url=f"{settings.API_PREFIX}/openapi.json",
    openapi_tags=settings.TAGS_METADATA,
    exception_handlers={status.HTTP_500_INTERNAL_SERVER_ERROR: error_handlers.handle_500_exception},
)
APP_OBJ.include_router(settings_views.ROUTER_OBJ, tags=['settings'])
APP_OBJ.include_router(main_views.ROUTER_OBJ, prefix=settings.API_PREFIX, tags=['main'])
APP_OBJ.include_router(debug_views.ROUTER_OBJ, prefix=settings.API_PREFIX, tags=['debug'])
APP_OBJ.include_router(get_health_check_router(), prefix=settings.API_PREFIX, tags=['health'])
APP_OBJ.add_middleware(log_middleware.RequestIDASGIMiddleware)
APP_OBJ.add_middleware(SentryAsgiMiddleware)

if settings.DEBUG:
    APP_OBJ.add_middleware(
        CORSMiddleware, allow_origins=["*"], allow_credentials=True, allow_methods=["*"], allow_headers=["*"],
    )


if __name__ == '__main__':
    uvicorn_server: Server = Server(
        Config(
            APP_OBJ,
            host='0.0.0.0',
            port=settings.UVICORN['APP_PORT'],
            log_level=settings.UVICORN['LOG_LEVEL'],
            workers=settings.UVICORN['APP_WORKERS'],
            loop=settings.UVICORN['APP_LOOP'],
            limit_max_requests=settings.UVICORN['APP_LIMIT_MAX_REQUESTS'],
        )
    )
    setup_json_logger(need_sentry=bool(settings.SENTRY_DSN))
    security.set_jwt_public_key(settings.JWT_PUBLIC_KEY)
    uvicorn_server.run()
