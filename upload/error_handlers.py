"""Custom error handlers."""
from fastapi import HTTPException, Request, status
from starlette.responses import JSONResponse

from upload import models


async def handle_500_exception(_: Request, exc: Exception) -> JSONResponse:
    """Handle only 500 exception."""
    if isinstance(exc, HTTPException):
        raise exc
    return JSONResponse(
        {'detail': models.BadResponse(error='Internal error.').dict()},
        status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
    )
