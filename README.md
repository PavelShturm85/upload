Platform-upload
===
This service accept multiple uploads, stores them in S3 and returns urls and various meta info.<br>
Also it provides admin interface and API.

## CLI
1. dev scripts (located in [./bin/scripts](./bin/scripts))
    * `./bin/scripts/build-docker.sh` — helper for docker building
    * `./bin/scripts/run.sh` — dev server
1. production scripts (located in [./bin/app](./bin/app))
    * `./bin/app/run.sh` — main application server


## Quickstart
* local development
    * install `brew install libmagic` for MacOS (or analog for other OS)
    * `pipenv install`
    * `./bin/scripts/run.sh`
* local k8s development
    * run `./bin/scripts.py s3_init` in iac repo for create bucket in s3


## Linting
* disabled `bad-continuation` in `.pylintrc` because of incompatibility with pylint and black


## Service API
1. check on url `/doc/api/service-upload/`


## ENV variables
Default:
* `PLATFORM_DEBUG` bool
* `PLATFORM_PUBLIC_API_PREFIX` default is `/api/upload/`
* `UPLOAD_DOC_PREFIX` default is `/doc/api/service-upload/`
* `UPLOAD_BULK_FILES_COUNT` default is 10
* `UPLOAD_MAXIMUM_FILE_SIZE_BYTES` default is 10mb

Necessary:
* `PLATFORM_S3_BUCKET`
* `PLATFORM_S3_WEB_SERVER_VISIBLE_URL`
* `PLATFORM_S3_ENDPOINT`
* `PLATFORM_S3_ACCESS_KEY`
* `PLATFORM_S3_SECRET_KEY`


## Kubernetes
* CLI `helm upgrade -i -f envs/local.yaml release_name k8s/.`
* Where:
    * `local` is your environment name
    * `release_name` is release name (you can use service name)
