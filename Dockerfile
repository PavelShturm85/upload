FROM python:3.9.1

RUN apt-get update && apt-get -y upgrade && apt-get install -y libmagic-dev --no-install-recommends

COPY Pipfile* ./
RUN pipenv install --dev --system --deploy --ignore-pipfile

RUN arti-pip -a end
RUN apt-get remove -y gcc cmake make && rm -rf /var/lib/apt/lists/* && apt-get autoremove -y && apt-get clean
RUN pip uninstall pipenv -y

COPY . .


FROM scratch AS runtime-image

ENV REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca-certificates.crt \
    LANG="C.UTF-8" \
    PYTHON_VERSION=3.9.1 \
    PYTHONUNBUFFERED=1 \
    WORKDIR=/srv/www/
WORKDIR $WORKDIR

COPY --from=compile-image / /
